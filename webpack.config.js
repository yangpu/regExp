const HtmlWebpackPlugin = require('html-webpack-plugin'); // 生成index.html模板自动插入打包后的script
const CleanWebpackPlugin = require('clean-webpack-plugin'); // 构建前清理 /dist 文件夹
const path = require('path');
const webpack = require('webpack')
module.exports = {
  mode: 'development',
  devtool: 'eval-source-map',
  entry: path.resolve(__dirname, 'src/chapter_01/index.js'),

  output: {
    path: path.resolve(__dirname, 'dist'), // 打包后存放的文件夹
    filename: 'app.js' // 打包后的文件
  },

  devServer: {
    contentBase: path.resolve(__dirname, 'dist'),
    port: 9000,
    host: 'localhost'
  },

  plugins: [
    new CleanWebpackPlugin([path.resolve(__dirname, 'dist')]),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      title: 'regExp',
      hash: true
    }) 
  ]
}